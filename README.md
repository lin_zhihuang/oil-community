# oil社区

## 技术栈

微信小程序+ 云开发 + colorUI + vant weapp

## 开发规范

### 目录结构
```
oil-community 
├── cloudfunctions 云函数目录
│   ├── post
│   └── user
└── miniprogram 
    ├── components 通用组件目录
    ├── lib 无关业务通用方法目录
    ├── miniprogram_npm npm包
    ├── pages 所有页面
    ├── static  静态文件目录
    ├── template 页面模板目录
    └── utils 业务相关通用方法目录
```

### mixins
小程序使用了`mixins`混入的方式进行模块化，使用方式与`vue.js`相同，实现代码请看`mixins.js`

### require
开发时请遵循`commonjs`模块规范

为了避免引入通用方法路径过长，在`app.js`中定义了`app.require()`方法
```js
App({
  require: (path) => require(path)
})
```

在页面中使用时，调用方法如下
```js
const app = getApp()
const upload = app.require("lib/upload")
```

### utils
与业务相关的通用方法都放在*utils*文件夹中
需要参数且需要复用的路由跳转放在 `utils/router` 目录中对应的模块内
需要参数且需要复用的请求放在 `utils/request` 目录中对应的模块内

### lib
与业务无关的通用方法都放在*lib*文件夹中

异步方法如果需要处理错误使用 `lib/awaitTo.js` 进行处理，具体使用方式如下

```js
const app = getApp()
const to = app.require("lib/awaitTo")

const [err, res] = await to(
    // Async function
)
    if (err) {
        // handle err
    }
    // do somethinig
``` 

## 参考文档

- [云开发文档](https://developers.weixin.qq.com/miniprogram/dev/wxcloud/basis/getting-started.html)

