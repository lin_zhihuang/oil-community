module.exports = {
    showUserPage(userId) {
        // 跳转至个人主页
        wx.navigateTo({
            url: `/pages/user-page/user-page?userId=${userId}`
        })
    },
    showUserList({ title, type }) {
        wx.navigateTo({
            url: `/pages/user-list/user-list?type=${type}&title=${title}`,
        })
    }
}