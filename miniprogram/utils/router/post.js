module.exports = {
  showPostDetail(postId) {
    // 跳转至文章详情
    wx.navigateTo({
      url: `/pages/post-detail/post-detail?postId=${postId}`,
    })
  },
  showPostList({ title, type }) {
    wx.navigateTo({
      url: `/pages/post-list/post-list?type=${type}&title=${title}`,
    })
  }
}