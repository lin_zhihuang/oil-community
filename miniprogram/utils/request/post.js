const db = wx.cloud.database()
const _ = db.command
const $ = db.command.aggregate
module.exports = {
    getPost(match, lastTime = new Date().getTime()) {
        return db.collection("post")
            .aggregate()
            .match({ createTime: _.lt(lastTime), ...match })
            .project({
                favorNum: $.size('$favor'),
                user: { nickName: true, avatarUrl: true },
                topic: true,
                title: true,
                content: true,
                imgList: true,
                viewNum: true,
                createTime: true,
                userId: true
            })
            .sort({ createTime: -1 })
            .limit(10)
            .end()
    }
}