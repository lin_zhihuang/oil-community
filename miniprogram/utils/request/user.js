
const db = wx.cloud.database()
const _ = db.command
const $ = db.command.aggregate
module.exports = {
    getUser(userId) {
        const getUserInfo = db.collection("user")
            .doc(userId)
            .get()
        // 获取用户发布数量
        const getPostNum = db.collection("post")
            .where({ userId })
            .count()

        // 获取用户获赞总数
        const getFavorNum = db.collection("post").aggregate()
            .match({ userId })
            .group({
                _id: null,
                favorNum: $.sum('$favorNum')
            })
            .end()
        return Promise.all([getUserInfo, getPostNum, getFavorNum])
    },
    getUserList(match = {}, project = {}, lastTime = new Date().getTime()) {
        return db.collection("user")
            .aggregate()
            .project({
                ...project,
                fansNum: true,
                avatarUrl: true,
                intro:true,
                nickName: true,
                createTime: true
            })
            .match({ createTime: _.lt(lastTime), ...match })
            .sort({ createTime: -1 })
            .limit(10)
            .end()
    }
}