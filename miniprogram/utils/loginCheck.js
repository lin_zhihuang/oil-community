const { showConfirm } = require("../lib/modal")
const homePath = "pages/home/home"
module.exports = async function () {
    const user = wx.getStorageSync('user')
    if (!user) {
        const res = await showConfirm("登录解锁更多功能~", "去登陆")
        if (!res) { return true }
        const pages = getCurrentPages()
        const curPage = pages[pages.length - 1]
        if (curPage.route !== homePath) {
            await wx.redirectTo({
                url: '/' + homePath,
            })
        }
        wx.showToast({
            title: '左上角点击登录',
            icon: "none"
        })
        return true
    }
    return false
}