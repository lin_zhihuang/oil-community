// 防抖函数
module.exports = function debounce(func, delay, iFunc = () => { }) {
	let timer
	return function (...args) {
		iFunc.apply(this)
		clearTimeout(timer)
		timer = setTimeout(() => {
			func.apply(this, args)
		}, delay)
	}
}