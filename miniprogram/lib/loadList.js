const dayjs = getApp().require("lib/day")
module.exports = {
    listData: {
        list: [],
        lastTime: new Date().getTime(),
        nomore: false
    },
    loadData({ resData, timeFormat = "from", targetObj, loadType = "getData" }) {
        const { list } = this.data[targetObj]
        if (resData.length <= 10) {
            this.setData({ [`${targetObj}.nomore`]: true })
        }
        resData = resData.map(item => {
            item.createTime = dayjs(item.createTime)[timeFormat](timeFormat === "from" ? new Date() : "YYYY/MM/DD")
            return item
        })
        this.setData({
            [`${targetObj}.list`]: loadType === "getData" ? list.concat(resData) : resData,
            [`${targetObj}.lastTime`]: resData.length && resData[resData.length - 1].createTime
        })
    },
    refresh(targetObj) {
        this.setData({ [`${targetObj}.nomore`]: false, [`${targetObj}.lastTime`]: new Date().getTime() })
    }
}