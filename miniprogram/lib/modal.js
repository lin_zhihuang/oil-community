// 通用弹窗方法
const config = getApp().globalData.config
module.exports = {
    // 弹出确认框，有取消按钮
    async showConfirm(title, confirmText = "确认") {
        const res = await wx.showModal({
            title,
            cancelColor: '#b6b6b6',
            confirmText,
            confirmColor: config.primaryColor,
        })
        if (res.cancel) {
            return false
        } else if (res.confirm) {
            return true
        }
    },
    // 弹出提示框，无取消按钮
    async showMessage(content, confirmText = "确认") {
        await wx.showModal({
            title: '提示',
            content,
            showCancel: false,
            confirmText,
            confirmColor: config.primaryColor,
        })

    }

}