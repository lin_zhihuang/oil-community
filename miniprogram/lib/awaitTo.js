
module.exports = function to(promise, description = "unknown") {
	const pages = getCurrentPages()
	const route = pages[pages.length - 1].route || 'unknown'
	description = `[${route}]---[${description}]`

	console.time(description)


	return promise
		.then(function (data) {
			console.timeEnd(description)
			return [null, data];
		})
		.catch(function (err) {
			wx.showToast({
				title: '请求失败',
				icon: "none"
			})
			return [err, undefined];
		});
}


