// 调用云函数
module.exports = async function cloudFunc(path, param) {
	const name = path.split('/')[0]
	const url = path.split('/')[1]
	const res = await wx.cloud.callFunction({
		name,
		data: {
			...param,
			url
		},
		parse: true
	})
	return res
}