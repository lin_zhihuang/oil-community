// 获取小程序节点
module.exports = function getDom(target, ctx) {
	return new Promise((res, rej) => {
		const query = wx.createSelectorQuery().in(ctx)
		query.select(target).boundingClientRect(data => {
			res(data)
		}).exec()
	})
}

