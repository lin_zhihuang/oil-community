// day.js引入配置并导出
import dayjs from './dayjs/index.js'
import './dayjs/locale/zh-cn.js'
import rTime from './dayjs/plugin/relativeTime/index.js'
dayjs.locale('zh-cn')
// 使用插件。固定格式dayjs.extend(插件)
dayjs.extend(rTime)
module.exports = dayjs
