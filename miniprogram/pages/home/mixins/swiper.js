const collection = require("../../post-detail/mixins/collection")

const db = wx.cloud.database()
module.exports = {
	data: {
		swiper: {
			active: 0,
			list: []
		}
	},
	onLoad() {
		this.getSwiper()
	},
	async getSwiper() {
		// 获取轮播图数据
		const res = await db.collection("swiper").get()
		this.setData({
			"swiper.list": res.data
		})
	},
	swiperChange(e) {
		// 轮播图切换
		this.setData({
			"swiper.active": e.detail.current
		})
	},

}