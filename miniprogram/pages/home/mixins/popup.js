module.exports = {
    data:{
        popupFlag: false,
    },
    hidePopup() {
		this.setData({ "popupFlag": false })
	},
	showPopup() {
		this.setData({ "popupFlag": true })
	},
}