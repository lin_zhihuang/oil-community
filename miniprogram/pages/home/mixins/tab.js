const app = getApp()
const getDom = app.require("lib/getDom")
module.exports = {
	data: {
		tab: {
			active: 0,
			list: [
				{ type: "newest", label: "最新" },
				{ type: "follow", label: "关注" },
			],
			tabHeight: 0
		}

	},
	async onLoad() {
		const res = await getDom(".card-swiper", this)
		this.data.tab.tabHeight = res.bottom
	},
	async tabChange() {
		this.onPullDownRefresh()
		wx.pageScrollTo({
			scrollTop: this.data.tab.tabHeight - this.data.topBarHeight,
			duration: 300,
		})
	},

}