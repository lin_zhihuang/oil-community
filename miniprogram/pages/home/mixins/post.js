const app = getApp()

const to = app.require("lib/awaitTo")
const cloudFunc = app.require("lib/cloudFunc")
const { listData, loadData, refresh } = app.require("lib/loadList")
module.exports = {
	data: {
		post: listData
	},
	async onLoad() {
		this.getPost()
	},
	async getPost(loadType = "getData") {
		const type = this.data.tab.list[this.data.tab.active].type
		const lastTime = this.data.post.lastTime
		if (type === "newest") {
			const { getPost } = app.require("utils/request/post")
			var [err, res] = await to(getPost({}, lastTime), "getPost")
		}
		else {
			var [err, res] = await to(cloudFunc("post/getFollowPost", { userId: this.data.user._id || '' }), "getFollowPost")
		}
		if (err) { return }
		loadData.call(this, {
			resData: res.list,
			targetObj: "post",
			loadType
		})
	},
	onReachBottom() {
		// 触底加载
		if (this.data.post.nomore) return
		this.getPost()
	},
	async onPullDownRefresh() {
		// 下拉刷新
		refresh.call(this, "post")
		await this.getPost("refresh")
		wx.stopPullDownRefresh()
	}
}