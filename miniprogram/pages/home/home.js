// 引入方法
const app = getApp()
const debounce = app.require("lib/debounce")
const loginCheck = app.require("utils/loginCheck")
// mixins模块
const swiper = require("./mixins/swiper")
const tab = require("./mixins/tab")
const popup = require("./mixins/popup")
const user = require("./mixins/user")
const post = require("./mixins/post")
const statusBarHeight = getApp().globalData.statusBarHeight
Page({
	mixins: [swiper, tab, popup, user, post],
	data: {
		config: app.globalData.config,
		statusBarHeight,
		topBarHeight: 90,
		btnFlag: true,
		unreadNum: app.globalData.message.unreadNum
	},
	async onLoad() {
		// 收到消息时设置未读提醒
		app.globalData.pubSub.subscribe("onMessage", () => {
			this.setData({ unreadNum: app.globalData.message.unreadNum })
		})
		// 用户数据更新
		app.globalData.pubSub.subscribe("onUserUpdate", () => {
			this.setData({ user: wx.getStorageSync('user') })
		})
		// 退出登录时清除用户及关闭左侧栏
		app.globalData.pubSub.subscribe("onLogout", () => {
			this.setData({ user: null, popupFlag: false })
		})
	},
	showSearch() {
		// 跳转至搜索页面
		wx.navigateTo({
			url: '../search/search',
		})
	},
	async showForm() {
		// 跳转至发布表单
		if (await loginCheck()) { return }
		wx.navigateTo({
			url: '../form/post-form/post-form',
		})
	},
	showPost(e) {
		// 跳转至文章详情
		const post = e.currentTarget.dataset.post
		const { showPostDetail } = app.require("utils/router/post")
		showPostDetail(post._id, post.userId)
	},
	onPageScroll: debounce(function () {
		this.setData({ btnFlag: true })
	}, 400, function () {
		if (this.data.btnFlag) {
			this.setData({ btnFlag: false })
		}
	})
})