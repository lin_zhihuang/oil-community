const app = getApp()

module.exports = {
    data: {
        history: []
    },
    initHistory() {
        const history = wx.getStorageSync('history')
        if (!history) {
            wx.setStorageSync('history', [])
        }
        else {
            this.setData({ history })
        }
    },
    addHistory(content) {
        const history = this.data.history
        const index = history.indexOf(content)
        if (index !== -1) {
            history.splice(index, 1)
        }
        history.unshift(content)
        this.setData({ history, "search.flag": false })
        wx.setStorageSync('history', history)
    },
    async clearHistory() {
        const { showConfirm } = app.require("lib/modal")
        const res = await showConfirm("清除所有历史搜索记录")
        if (!res) {
            return
        }
        wx.setStorageSync('history', [])
        this.setData({ history: [] })
    },
    chooseHistory(e) {
        this.setData({ "search.content": e.currentTarget.dataset.value })
        this.searchWord()
    }
}