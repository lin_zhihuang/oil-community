const app = getApp()
const db = wx.cloud.database()
const _ = db.command
const $ = db.command.aggregate
const to = app.require("lib/awaitTo")
const { listData, loadData, refresh } = app.require("lib/loadList")
Page({
    data: {
        me: wx.getStorageSync('user'),
        type: "",
        user: listData
    },
    onLoad(e) {
        this.setData({ type: e.type })
        wx.setNavigationBarTitle({
            title: e.title,
        })
        this.getUser()
    },
    showUserPage(e) {
        const userId = e.currentTarget.dataset.userId
        // 跳转用户主页
        const {
            showUserPage
        } = app.require("utils/router/user")
        showUserPage(userId)
    },
    async getUser(loadType = "getData") {
        const map = {
            myFollow: {
                project: { isFollow: $.in([this.data.me._id || '', '$fans']), }, match: {
                    isFollow: true
                }
            }
        }
        const lastTime = this.data.user.lastTime
        const { getUserList } = app.require("utils/request/user")
        const [err, res] = await to(getUserList(map[this.data.type].match, map[this.data.type].project, lastTime), "getUser")
        console.log(res)
        if (err) { return }
        loadData.call(this, {
            resData: res.list,
            targetObj: "user",
            loadType
        })
    },
    onReachBottom() {
        if (this.data.user.nomore) return
        this.getUser()
    },
    async onPullDownRefresh() {
        refresh.call(this, "user")
        await this.getUser("refresh")

        wx.stopPullDownRefresh()
    }
})